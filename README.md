![Build Status](https://gitlab.com/pages/hugo/badges/master/build.svg)

---

Página web del Concilio de lo Libre, construida con [Hugo] usando GitLab Pages.

[hugo]: https://gohugo.io

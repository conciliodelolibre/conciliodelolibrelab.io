---
title: Materiales docentes en abierto
subtitle: Charla sobre publicación de materiales docentes en abierto
date: 2021-11-15
tags: ["charla"]
---

Muchos de los materiales docentes que producimos en la Universidad podrían publicarse en acceso abierto (open access). La publicación en abierto de materiales docentes puede tener muchas ventajas para tanto para profesores como alumnos. Los materiales se pueden compartir sin problemas, se pueden actualizar o reelaborar, y el trabajo colaborativo se hace mucho más fácil. La visibilidad de estos materiales, y por tanto de sus autores, también es mayor. Pero no todo son ventajas: también pueden aparecer algunos problemas. Por ello, es conveniente conocer con cierto detalle las implicaciones de esta forma de distribuir nuestros materiales, de forma que podamos maximizar sus ventajas, y reducir al mínimo los problemas que nos pueda causar.

Para conocer estas implicaciones, tenemos que revisar por un momento cómo se aplica la legislación de propiedad intelectual, cómo se trata el concepto de autoría, qué licencias de distribución tenemos que usar, y de qué manera expresamos en nuestros materiales que los estamos publicando en abierto. Así que toca hablar de todos estos asuntos, y de algunos más: cada tipo de materiales (materiales bibliográficos, imágenes, audios, videos, programas de ordenador, etc.) tiene sus propias particularidades.

Por eso, desde la [OfiLibre](https://ofilibre.gitlab.io/) de la Universidad Rey Juan Carlos, el martes 16 de noviembre, de 13 a 14, exploraremos las razones que podemos tener para hacerlo y las ventajas y problemas que ésto puede conllevar. Pero sobre todo exploraremos en detalle qué implica publicar en abierto y qué tenemos que hacer con nuestros materiales si queremos publicarlos en abierto. La presentación será mediante videoconferencia y puedes encontrar detalles sobre cómo conectarte en la [página de información sobre el evento](https://eventos.urjc.es/74286/detail/publicacion-de-materiales-docentes-en-acceso-abierto.html).

Así que si quieres saber un poco más sobre publicación en abierto, sobre licencias libres, sobre Creative Commons, sobre localización de recursos reutilizables, sobre archivos abiertos… ¡vente!


---
title: ¡Ya tenemos página web!
subtitle: Todo llega...
date: 2021-11-05
tags: ["saludos"]
---

Inauguramos la página web y el blog del Concilio de lo Libre con esta primera entrada. La página está alojada en GitLab, con GitLab pages, y usamos [Hugo](https://gohugo.io/hosting-and-deployment/hosting-on-gitlab/) para maquetarla. Esto nos proporciona una página estática que hace de frontal y un blog con mínimo esfuerzo y todo basado en software libre. 

Esperamos que las actividades que vayamos visibilizando por aquí sean de nuestro interés.

¡Nos vemos en el Concilio de lo Libre!

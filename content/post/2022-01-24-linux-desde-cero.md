---
title: Linux desde cero
subtitle: Curso Online de Linux desde cero
date: 2022-01-24
tags: ["curso"]
---

La [Oficina de Software Libre de la Universidad de Zaragoza](https://osluz.unizar.es/) presenta el curso a distancia [Linux desde cero](https://osluz.unizar.es/content/curso-online-de-linux-desde-cero), dirigido a cualquier persona que tenga interés en comenzar a utilizar Linux, por curiosidad o por necesidad, sin ningún tipo de conocimiento previo. Si eres un usuario de Windows que quiere migrar a Linux o si has oído hablar de Linux y lo quieres probar o si tienes algún ordenador o tablet antiguo al que le quieres dar una nueva vida, este es tu curso.

La primera sesión del curso es el próximo viernes, 28 de enero de 2022 a las 13 horas, y es preciso registrarse previamente. Datos y detalles en el [anuncio detallado del curso](https://osluz.unizar.es/content/curso-online-de-linux-desde-cero).

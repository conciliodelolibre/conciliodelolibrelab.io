---
title: Software libre para videoconferencias
subtitle: Dos profesionales del área nos presentan sus respectivas soluciones y planes de futuro
date: 2021-11-22
tags: ["charla"]
---

En un mundo donde nos hemos habituado a usar soluciones de videoconferencia, es necesario conocer las soluciones de código abierto que existen y tenemos a nuestra disposición. Si además dichas soluciones siguen un mismo estándar, mucho mejor. 

Para dar a conocer algunas de estas soluciones y estándares, la [OfiLibre](https://ofilibre.gitlab.io) de la Universidad Rey Juan Carlos y el [Concilio de lo Libre](https://conciliodelolibre.gitlab.io/) queremos anunciaros que el viernes 26 de noviembre de 13 a 14 tenemos una charla online sobre el estándar abierto WebRTC y las soluciones de código abierto que lo implementan y que podemos usar para videoconferencia.

En la charla contaremos con dos ponentes que conocen en detalle los retos de este tipo de aplicaciones: 

* Micael Gallego, profesor de la URJC y líder del proyecto [OpenVidu](https://openvidu.io/), una solución de código abierto basada en [WebRTC](https://webrtc.org/) pero que está especialmente orientada a embeber de forma sencilla videoconferencia en cualquier aplicación web moderna a través de librerías para los frameworks de desarrollo web más populares.

* Sergio Murillo, CTO de Millicast, Media Server Lead en CoSMo Software y fundador de [Medooze](https://github.com/medooze) un servidor de media de código abierto que implementa el estándar de WebRTC. Sergio es experto en WebRTC y conoce en detalle los entresijos del estándar. Además participa en la elaboración del nuevo estándar WHIP del IETF.

La charla será 100% online, y la realizaremos a través de BBB, otra solución de videoconferencia de código abierto ;)

El enlace para la conexión lo tienes disponible en la [página del evento](https://eventos.urjc.es/74373/detail/software-libre-para-videoconferencias.-seminarios-ofilibre.html). ¡Anímate!

![Charla software libre para videoconferencias](/images/blog/videoconferencia/cartel-sw-libre-videoconferencia.jpg)

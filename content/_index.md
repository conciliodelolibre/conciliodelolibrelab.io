## Concilio de lo Libre

El Concilio de lo Libre es una agrupación de oficinas de software libre y temas afines (publicación abierta, cultura y conocimiento libres) de diferentes universidades, que persiguen promover la cultura, el conocimiento y el software libres. Sin embargo, el concilio está abierto a cualquiera que tenga interés en conocer y dar a conocer estos temas, mediante la participación en los eventos que organizamos.

En nuestras respectivas instituciones organizamos eventos alrededor del conocimiento, la cultura y el software libre, que promocionamos a través del concilio, de forma que todos los que pertenecemos al concilio contribuimos a visibilizar estos eventos, que están dirigidos al público en general. Muchos de ellos se desarrollan de forma online, así que os invitamos a que os suscribáis al blog para estar al día de las cosas que organizamos.
